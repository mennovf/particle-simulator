/*
 * =====================================================================================
 *
 *       Filename:  directionalAttractor.cpp
 *
 *    Description:  :
 *
 *        Version:  1.0
 *        Created:  10/12/2012 18:00:43
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "directionalAttractor.hpp"


DirectionalAttractor::DirectionalAttractor (sf::Vector2f direction, float strength): Attractor{Attractor::Type::DIRECTIONAL, strength}, m_direction{direction}{
} 


sf::Vector2f DirectionalAttractor::getAcc(sf::Vector2f pos) const{
    return static_cast<int>(m_on) * m_strength * m_direction;
}

void DirectionalAttractor::changeDirection(sf::Vector2f dir){
    m_direction = dir;
}
