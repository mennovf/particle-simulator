#ifndef DIRECTIONALATTRACTOR_HPP_FWSZ2WNZ
#define DIRECTIONALATTRACTOR_HPP_FWSZ2WNZ

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "attractor.hpp"

class DirectionalAttractor : public Attractor{
public:
    DirectionalAttractor (sf::Vector2f, float);
    virtual ~DirectionalAttractor() override{};

    virtual sf::Vector2f getAcc(sf::Vector2f pos) const override;
    void changeDirection(sf::Vector2f);

private:
    sf::Vector2f m_direction;
};


#endif /* end of include guard: DIRECTIONALATTRACTOR_HPP_FWSZ2WNZ */
