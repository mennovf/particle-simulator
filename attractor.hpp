#ifndef ATTRACTOR_HPP
#define ATTRACTOR_HPP
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


//Abstract base class
class Attractor{
public:
    enum Type{
        NONE,
        POINT,
        DIRECTIONAL
    };

    Attractor (unsigned int type, float str): m_strength{str}, m_on{true}, m_type{type}{};
    virtual ~Attractor(){};

    //argument is the position of another object
    //returns the acceleration caused by this attractor
    virtual sf::Vector2f getAcc(sf::Vector2f) const = 0;
    virtual void changePos(sf::Vector2f){};
    virtual void changeState(bool state){m_on = state;};
    virtual void increaseStrength(float strength){m_strength += strength;};
    virtual float strength(){return m_strength;};

    unsigned int type(){return m_type;};
    bool isOn(){return m_on;};
   
protected: 
    float m_strength;
    bool m_on;
    const unsigned int m_type;
};


#endif /* end of include guard: ATTRACTOR_HPP */
