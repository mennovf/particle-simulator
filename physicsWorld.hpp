#ifndef PHYSICSWORLD_HPP_IPA4YHNT
#define PHYSICSWORLD_HPP_IPA4YHNT
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <memory>
#include <vector>
#include <functional>

#include "attractor.hpp"
#include "directionalAttractor.hpp"

class PhysicsWorld{
public:
    PhysicsWorld (const int, const int);
    ~PhysicsWorld () = default;

    void addAttractor(sf::Vector2f pos, float acc = 800.0f);
    void changeAttractorPos(unsigned int, sf::Vector2f);
    void changeAttractorState(unsigned int, bool);
    unsigned int attractorsSize(){return m_attractors.size();};
    void increaseAttractorStrength(unsigned int i, float strength){m_attractors[i]->increaseStrength(strength);};
    float getStrength(unsigned int);

    void clearAttractors(){m_attractors.clear();};

    void setGravity(bool);
    bool getGravity();
    void increaseGrav(float a){m_g.increaseStrength(a);};
    float getGrav(){return m_g.strength();};

    void updateAll(std::vector<sf::Vector2f>&, std::vector<sf::Vector2f>& , sf::Time);
protected:

    std::vector<std::unique_ptr<Attractor>> m_attractors;
    
    //gravitational acceleration
    DirectionalAttractor m_g;
    bool m_gravity;
    //air friction between 0 and 1;
    float m_dampening;
    int m_xBound, m_yBound;

    std::function<float()> m_rGen;
};


#endif /* end of include guard: PHYSICSWORLD_HPP_IPA4YHNT */
