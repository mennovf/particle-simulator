#include "particleContainer.hpp"
#include "utils.hpp"

#include <cmath>
#include <utility>

ParticleContainer::ParticleContainer(const int xBound, const int yBound, std::unique_ptr<PhysicsWorld> phys) :
     m_pos{}, m_vel{}, m_acc{}, m_phys{std::move(phys)}, m_xBound{xBound}, m_yBound{yBound}, m_pSize{2}, m_tex{}, m_spr{}{
        sf::Image img;
        img.create(m_pSize, m_pSize , sf::Color(30, 7, 4));
        m_tex.loadFromImage(img);

        m_spr.setTexture(m_tex);
        m_spr.setPosition(0, 0);
    }

void ParticleContainer::add_particle(sf::Vector2f pos){
    sf::Vector2f acc{0, 0};
    add_particle(pos, acc);
}

void ParticleContainer::add_particle(sf::Vector2f pos, sf::Vector2f acc){
    m_pos.push_back(pos);
    m_vel.push_back(sf::Vector2f{0, 0});
    m_acc.push_back(acc);
}

void ParticleContainer::update(sf::Time time){
    m_phys->updateAll(m_vel, m_pos, time);
}


sf::Sprite ParticleContainer::getSprite(const unsigned int i){
    m_spr.setPosition(m_pos[i].x, m_pos[i].y);
    return m_spr;
}

void ParticleContainer::clear(){
    m_pos.clear();
    m_vel.clear();
}

