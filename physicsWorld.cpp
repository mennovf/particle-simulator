/*
 * =====================================================================================
 *
 *       Filename:  physicsWorld.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  30/11/2012 15:54:28
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "physicsWorld.hpp"
#include "pointAttractor.hpp"
#include "utils.hpp"

#include <iostream>
#include <random>
#include <functional>


PhysicsWorld::PhysicsWorld(const int x, const int y): 
    m_attractors{},
    m_g{sf::Vector2f{0, 1.f}, 400.f}, 
    m_gravity{true},
    m_dampening{0.8f},
    m_xBound{x}, m_yBound{y}, 
    m_rGen{std::bind(std::uniform_real_distribution<float>{-4.f, 4.f}, std::mt19937())}{
}

void PhysicsWorld::addAttractor(sf::Vector2f pos, float acc){
    m_attractors.push_back(std::unique_ptr<PointAttractor>{new PointAttractor(pos, acc)});
}


void PhysicsWorld::changeAttractorPos(unsigned int i , sf::Vector2f pos){
    m_attractors[i]->changePos(pos);
}

void PhysicsWorld::changeAttractorState(unsigned int i, bool state){
    m_attractors[i]->changeState(state);
}

void PhysicsWorld::updateAll(std::vector<sf::Vector2f>& vel, std::vector<sf::Vector2f>& pos, sf::Time elapsedTime){
    for (unsigned int i = 0; i < pos.size(); i++){
        float passedTime = elapsedTime.asSeconds();
        
        //New acceleration
        sf::Vector2f acc = m_g.getAcc(sf::Vector2f{0,0});
        sf::Vector2f oldPos = pos[i];
        for (auto& attractor : m_attractors){
            acc += attractor->getAcc(oldPos);
        }

        //Entropy!: For seperating little clusters
        if(mine::magnitude(acc)){
            acc += sf::Vector2f{m_rGen(), m_rGen()};
        }
        
        //New Velocity
        vel[i] += (acc * passedTime) * m_dampening;

        //New Position
        sf::Vector2f newPos = pos[i] + vel[i] * passedTime;

        ////Make sure the new position is within the bounds
        //To be replaced later by a dynamic collision system
        //Luch like the attractors
        sf::Vector2f cappedPos;
        cappedPos.x = mine::max( 0.f, mine::min(newPos.x, (float)m_xBound-1) );
        cappedPos.y = mine::max( 0.f, mine::min(newPos.y, (float)m_yBound-1) );
        if (cappedPos.x != newPos.x) vel[i].x = 0;
        if (cappedPos.y != newPos.y) vel[i].y = 0;

        pos[i] = cappedPos;
    }
}

void PhysicsWorld::setGravity(bool on){
    m_g.changeState(on); 
}

bool PhysicsWorld::getGravity(){
    return m_g.isOn();
}

float PhysicsWorld::getStrength(unsigned int i){
    return m_attractors[0]->strength();
}
