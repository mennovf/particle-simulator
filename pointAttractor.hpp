#ifndef POINTATTRACTOR_HPP
#define POINTATTRACTOR_HPP
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "attractor.hpp"

class PointAttractor : public Attractor{
public:
    PointAttractor (sf::Vector2f, float);
    PointAttractor (sf::Vector2f);
    virtual ~PointAttractor () override{};

    virtual sf::Vector2f getAcc(sf::Vector2f pos) const override;
    virtual void changePos(sf::Vector2f) override;
    virtual void increaseStrength(float) override;
private:
    sf::Vector2f m_pos;
    //Strength should be around the 5000, it drops of quick because of the
    //1/r�
};


#endif /* end of include guard: POINTATTRACTOR_HPP */
