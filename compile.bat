g++ -std=c++0x -DSFML_DYNAMIC -O4 -mwindows -Wall -Wextra -Wno-reorder -pedantic ^
main.cpp psim.cpp particleContainer.cpp physicsWorld.cpp eventClass.cpp ^
pointAttractor.cpp directionalAttractor.cpp ^
utils.cpp ^
-static-libgcc -static-libstdc++ -lsfml-graphics-2 -lsfml-window-2 -lsfml-system-2
pause
