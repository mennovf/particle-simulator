#ifndef PSIM_HPP
#define PSIM_HPP
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>

#include "eventClass.hpp"
#include "particleContainer.hpp"
#include "physicsWorld.hpp"

class Psim : public EventClass{
public:
    Psim (const int, const int);
    virtual ~Psim () = default;

    void run();
protected:
    void render();
    void renderInfo();
    void renderInstructions();
    void update(sf::Time elapsedTime);
    void clearAttractors();

    void dumpParticles();


    virtual void handleRightDrag(sf::Event event) override;
    virtual void handleClose(sf::Event event) override;

    virtual void handleLeftClick(sf::Event event) override;
    virtual void handleLeftRelease(sf::Event event) override;
    virtual void handleLeftDrag(sf::Event event) override;

    virtual void handleKeyRelease(sf::Event event) override;
    virtual void handleScroll(sf::Event event) override;


    float getFps();

    sf::RenderWindow m_window;
    sf::RenderTexture m_rTexture;
    sf::Font m_font;

    ParticleContainer m_particles;
    PhysicsWorld m_phys;

    int m_windowX, m_windowY;
    unsigned int m_pRate;
    bool m_running;
    
    bool m_showInstructions;
    std::vector<std::string> m_instructions;
};


#endif 
