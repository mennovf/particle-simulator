#ifndef UTILS_HPP_CBBE6S2W
#define UTILS_HPP_CBBE6S2W
 /* end of include guard: UTILS_HPP_CBBE6S2W */

/*
 *
 * =====================================================================================
 *
 *       Filename:  utils.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  6/11/2012 22:09:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <sstream>
#include <string>
namespace mine{
    float magnitude(sf::Vector2f& vec);
    sf::Vector2f scalarMul(const sf::Vector2f& vec, float scalar);
    sf::Vector2f atob(sf::Vector2f, sf::Vector2f);
    sf::Vector2f itof(sf::Vector2i);

    template<typename T>
        std::string toString(T toConvert){
            std::stringstream ss;
            ss << toConvert;
            return ss.str();
        }

    template<typename T>
        T max(T one, T two){
            return (one > two)? one : two;
        }
    template<typename T>
        T min(T one, T two){
            return (one < two)? one : two;
        }
} /* mine */ 
#endif
