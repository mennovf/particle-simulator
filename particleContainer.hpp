#ifndef PARTICLECONTAINER_HPP_UEML3HVS
#define PARTICLECONTAINER_HPP_UEML3HVS
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <memory>

#include "physicsWorld.hpp"

class ParticleContainer{
public:
    ParticleContainer (const int xBound, const int yBound, std::unique_ptr<PhysicsWorld> phys);
    ParticleContainer () = delete;

    void add_particle(sf::Vector2f pos);
    void add_particle(sf::Vector2f pos, sf::Vector2f acc);
    void clear();

    unsigned int size(){return m_pos.size();};
    sf::Vector2f getPos(unsigned int i){return m_pos[i];};

    void update(sf::Time time);
    sf::Sprite getSprite(const unsigned int i);

private:
    //repsectively positions, velocities, and accelerations of the particles
    std::vector<sf::Vector2f> m_pos, m_vel, m_acc;

    std::unique_ptr<PhysicsWorld> m_phys;
    //The x and y bound of the container
    const int m_xBound, m_yBound;
    const int m_pSize;

    sf::Texture m_tex;
    sf::Sprite m_spr;
};


#endif
