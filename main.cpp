#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include <cstring>

#include "psim.hpp"

int main(int argc, char const *argv[]){
    //Check if help is required
    for(int i = 0; i < argc; ++i){
        if (std::strcmp(argv[i], "-h") == 0 || std::strcmp(argv[i], "--help") == 0){
            //Read instructions from the file and print them to the console
            std::ifstream instructions{"instructions.txt"};
            if(!instructions.is_open()){
                std::cerr << "Unable to open instructions.txt, is it in this directory?" << std::endl;
                return -1;
            }

            //Read the contents and print them
            std::string linebuf;
            while(std::getline(instructions, linebuf)){
                std::cout << linebuf << std::endl;
            }
            
            instructions.close();
            return 0;
        }
    }

    //No help option present -> continue with the normal program
    int x = 640, y = 480;
    if(argc == 3){
        x = std::atoi(argv[1]);
        y = std::atoi(argv[2]);
    }

    Psim sim{x, y};
    sim.run();
    return 0;
}
