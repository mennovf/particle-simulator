g++ -std=c++0x -DSFML_STATIC -O3 -mwindows -Wall -Wextra -pedantic ^
-Wno-unused-parameter ^
main.cpp psim.cpp particleContainer.cpp physicsWorld.cpp eventClass.cpp ^
pointAttractor.cpp directionalAttractor.cpp ^
utils.cpp ^
-static-libgcc -static-libstdc++ -lsfml-graphics-s -lsfml-window-s -lsfml-system-s
pause
