#include "psim.hpp"
#include "utils.hpp"

#include <string>
#include <iostream>
#include <fstream>
#include <string>

//Psim::Psim(): EventClass(), m_windowX{640}, m_windowY{480}, m_window{sf::VideoMode(640, 480), "Psim", sf::Style::Close},
Psim::Psim(const int x, const int y): EventClass(), m_window{sf::VideoMode(x, y), "Psim", sf::Style::Close}, 
    m_rTexture{},
    m_font{},
    m_particles{m_windowX, m_windowY, std::unique_ptr<PhysicsWorld>{&m_phys}}, m_phys{x, y},
    m_windowX{x}, m_windowY{y}, 
    m_pRate{100}, 
    m_running{true},
    m_showInstructions{true}{

    m_font.loadFromFile("C:/Windows/Fonts/Arial.ttf");
    //intialise intermediate render texture
    m_rTexture.create(m_windowX, m_windowY);
    
    //Add the first attractor to the physics in the particle container
    //and put it's state to off, this attractor will serve as the mouse
    m_phys.addAttractor(sf::Vector2f{0.f, 0.f});
    m_phys.changeAttractorState(0, false);

    //Read the instructions and put them in a vector for each line
    std::ifstream instructions{"instructions.txt"};
    if(instructions){
        std::string line;
        for(int i = 0; std::getline(instructions, line); ++i){
            m_instructions.push_back(line);
        }
    }
}

void Psim::run(){
    sf::Clock clock;
    m_window.clear();
    while(m_running){
        sf::Event event;
        while(m_window.pollEvent(event)){
            handleEvent(event);
        }
        update(clock.restart());
        render();
    }
    m_window.close();
}

void Psim::render(){
    m_window.clear(sf::Color::Black);
    m_rTexture.clear(sf::Color::Black);

    //Render all particles
    const sf::RenderStates rState{sf::BlendMode::BlendAdd};
    for (unsigned int i = 0; i < m_particles.size(); i++){
        m_rTexture.draw(m_particles.getSprite(i), rState);
    }
    m_rTexture.display();
    m_window.draw(sf::Sprite{m_rTexture.getTexture()});
    

    renderInfo();
    if(m_showInstructions){
        renderInstructions();
    }
    //Update the screen
    m_window.display();

}

void Psim::renderInfo(){
    const int charSize = 10;
    const sf::Color normalCol = sf::Color::White;
    const sf::Color warningCol = sf::Color::Red;
    //Load tFps
    sf::Text tFps;
    tFps.setColor(normalCol);
    tFps.setStyle(sf::Text::Regular);
    tFps.setPosition(0, 0*charSize);
    tFps.setFont(m_font);
    tFps.setCharacterSize(charSize);

    //render fps
    std::string fps{"Fps: "};
    float ffps = getFps(); 
    if(ffps < 30.f) tFps.setColor(warningCol);
    fps.append(mine::toString(ffps));
    tFps.setString(sf::String{fps});
    m_window.draw(tFps);
    tFps.setColor(normalCol);

    //Render Particles
    std::string particles{"Particles: "};
    particles.append(mine::toString(m_particles.size()));
    tFps.setPosition(0, 1*charSize);
    tFps.setString(sf::String{particles});
    m_window.draw(tFps);

    //Render mouse attractor strength
    std::string strength{"Strenght: "};
    strength.append(mine::toString(m_phys.getStrength(0)));
    tFps.setPosition(0, 2*charSize);
    tFps.setString(sf::String{strength});
    m_window.draw(tFps);

    //Render gravity bool
    std::string gravbool{"Gravity: "};
    gravbool.append(m_phys.getGravity()? "on": "off");
    tFps.setPosition(0, 3*charSize);
    tFps.setString(sf::String{gravbool});
    m_window.draw(tFps);

    //Render grav strength
    std::string gravstrength{"g: "};
    gravstrength.append(mine::toString(m_phys.getGrav()));
    tFps.setPosition(0, 4*charSize);
    tFps.setString(sf::String{gravstrength});
    m_window.draw(tFps);

}

void Psim::renderInstructions(){
    const auto c = sf::Color::White;

    //Initialize an sf::Text object
    const auto charSize = 17;
    sf::Text instructionLine;
    instructionLine.setColor(c);
    instructionLine.setStyle(sf::Text::Regular);
    instructionLine.setFont(m_font);
    instructionLine.setCharacterSize(charSize);

    const auto xpos = m_windowX/10;
    const auto ypos = m_windowY/10;
    for(int i = 0; i<m_instructions.size(); ++i){
        const auto line = m_instructions[i];
        instructionLine.setString(sf::String{line});
        instructionLine.setPosition(xpos, ypos + i*charSize);
        m_window.draw(instructionLine);
    }
}

void Psim::update(sf::Time elapsedTime){
    m_particles.update(elapsedTime);
}

void Psim::handleRightDrag(sf::Event event){
    m_particles.add_particle(sf::Vector2f{static_cast<float>(event.mouseMove.x), static_cast<float>(event.mouseMove.y)});
}

void Psim::handleClose(sf::Event event){
    m_running = false;
}

void Psim::handleLeftClick(sf::Event event){
    sf::Vector2f mousePos{static_cast<float>(event.mouseButton.x), 
        static_cast<float>(event.mouseButton.y)};
    m_phys.changeAttractorPos(0, mousePos);
    m_phys.changeAttractorState(0, true);   
}

void Psim::handleLeftRelease(sf::Event event){
    m_phys.changeAttractorState(0, false);
}

void Psim::handleLeftDrag(sf::Event event){
    sf::Vector2f mousePos{static_cast<float>(event.mouseMove.x), static_cast<float>(event.mouseMove.y)};
    m_phys.changeAttractorPos(0, mousePos);
}

float Psim::getFps(){
    static sf::Clock clock;
    return 1.0 / clock.restart().asSeconds();
}

void Psim::clearAttractors(){
    m_phys.clearAttractors();
    
    //Readd the mouse attractor
    m_phys.addAttractor(sf::Vector2f{0.f, 0.f});
    m_phys.changeAttractorState(0, false);
    if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)){
        m_phys.changeAttractorPos(0, mine::itof(sf::Mouse::getPosition(m_window)));
        m_phys.changeAttractorState(0, true);
    }
}
void Psim::handleKeyRelease(sf::Event event){
    switch (event.key.code){
        case (sf::Keyboard::A): m_phys.addAttractor(mine::itof(sf::Mouse::getPosition(m_window)), m_phys.getStrength(0)); break;
        case (sf::Keyboard::Add): m_phys.increaseGrav(40.f); break;
        case (sf::Keyboard::C): m_particles.clear(); break;
        case (sf::Keyboard::D): dumpParticles(); break;
        case (sf::Keyboard::G): m_phys.setGravity(! m_phys.getGravity()); break;
        case (sf::Keyboard::H): m_showInstructions = !m_showInstructions; break;
        case (sf::Keyboard::Q): m_running = false; break;
        case (sf::Keyboard::R): clearAttractors(); break;
        case (sf::Keyboard::Subtract): m_phys.increaseGrav(-40.f); break;
        default: break;
    }
}


void Psim::handleScroll(sf::Event event){
    m_phys.increaseAttractorStrength(0, event.mouseWheel.delta * 40);
}

void Psim::dumpParticles(){
    //add amount particles to every pixel
    const int amount = 10000;
    const int parts = m_windowX * m_windowY / amount;
    for (int x = 0; x < m_windowX; x++){
        for (int y = 0; y < m_windowY; y++){
            if(!((y * m_windowX + x) % parts)){
                sf::Vector2f vec{static_cast<float>(x), static_cast<float>(y)};
                m_particles.add_particle(vec);
            }
        }
    }
}
