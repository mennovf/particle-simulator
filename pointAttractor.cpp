#include <cmath>
#include "pointAttractor.hpp"
#include "utils.hpp"

PointAttractor::PointAttractor (sf::Vector2f pos, float strength): Attractor{Attractor::Type::POINT, strength}, m_pos{pos}{
}

PointAttractor::PointAttractor (sf::Vector2f pos): PointAttractor{pos, 800.f}{
}

sf::Vector2f PointAttractor::getAcc(sf::Vector2f pos) const{
    if (! m_on){
        return sf::Vector2f{0.f, 0.f};
    }

    sf::Vector2f aToB = mine::atob(pos, m_pos);
    float magnitude = mine::magnitude(aToB);

    //make atob unitvector
    aToB /= magnitude;

    //Acceleration using Newtons formula G * m1 * m2 / r�
    //G*m2 = strength of attractor, and m1 is always 1 in the case of my particles
    float rSquared =  (std::pow(magnitude, 2) / 50000.f);
    rSquared = mine::max(1.f, rSquared);
    sf::Vector2f acc =  (aToB) * m_strength / rSquared;
    return acc;
}

void PointAttractor::changePos(sf::Vector2f pos){
    m_pos = pos;
}

void PointAttractor::increaseStrength(float str){
    m_strength += str;
}
