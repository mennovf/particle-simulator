/*
 * =====================================================================================
 *
 *       Filename:  utils.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  6/11/2012 22:11:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (MvF), None
 *   Organization:  
 *
 * =====================================================================================
 */
#include "utils.hpp"
#include <cmath>
namespace mine{
    float magnitude(sf::Vector2f& vec){
        return std::sqrt(std::pow(vec.x, 2) + std::pow(vec.y, 2));
    }

    sf::Vector2f scalarMul(const sf::Vector2f& vec, float scalar){
        return sf::Vector2f{vec.x * scalar, vec.y * scalar};
    }

    sf::Vector2f atob(sf::Vector2f a, sf::Vector2f b){
        return b - a;
    }

    sf::Vector2f itof(sf::Vector2i i){
        float x = i.x, y = i.y;
        return sf::Vector2f{x, y};
    }
} /* mine */ 
